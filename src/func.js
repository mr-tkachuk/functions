const getSum = (str1, str2) => {
  // add your implementation below
  let result = false;
  if ((typeof str1 != "string")||(typeof str2 != "string")) return result;
  if ((isNaN(+str1))||(isNaN(+str2))) return result;

  result =String(+str1 + +str2);
  
  return result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = 0;
  let commentsCount = 0;
  let result = ``;
  for (let post of listOfPosts){
    if (post.author == authorName) postsCount++;
    if (post.comments) {
      for (let comment of post.comments){
        if (comment.author == authorName) commentsCount++;
      }
    }
  }
  result = `Post:${postsCount},comments:${commentsCount}`;

  return result;
};

const tickets=(people)=> {
  // add your implementation below
  let result = "YES";
  let cash ={ '25': 0,
              '50': 0,
              '100': 0
            };
  for (let item of people) {
    if (item == 25)
    { 
      cash['25']++;
      continue;
    }
    if (item == 50){
      if (cash['25'] == 0) {
        result = 'NO';
        break;
      } else {
        cash['25']--;
        cash['50']++;
        continue;
      }
    }
    if (item == 100){
      if (cash['25'] == 0) {
        result = 'NO';
        break;
      } else if((cash['25'] < 3)&&(cash['50'] < 1)){
        result = 'NO';
        break;
      } else {
        if (cash['50'] > 0){
          cash['25']--;
          cash['50']--;
          cash['100']++;
        } else {
        cash['25']-=3;
        cash['100']++;
        }
      }
    }
  }

  return result;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
